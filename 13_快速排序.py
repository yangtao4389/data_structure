
def quick_sort(alist,start,end):
    """快速排序
    将数据分为两部分，一部分比另外一部分都要小，然后递归
    """

    # 基准点
    # 根据基准点比较，两端开始：大的往右，小的往左

    # 停止条件：
    if start >= end:
        return

    # 基准点取第一个
    # n = len(alist
    mid = alist[start]
    left = start
    right = end

    while left<right:
        # [3,4,6,1,2,5]
        while alist[right] > mid and left<right:
            # 循环找到第一个比中间值更小 的数
            right -= 1
        alist[left] = alist[right]
        #                          这里价格条件是不让left比right大
        while alist[left] < mid and left<right:
            left += 1
        alist[right] = alist[left]
    # 将该中间值放入该位置
    # print(left,right)
    alist[left] = mid

    # 左右两边依次递归
    # print(alist)

    # 不能直接传入列表，这样都从0开始，就不能恢复原先列表
    quick_sort(alist,0,left-1)
    quick_sort(alist,left+1,end)

alist =	[3,4,6,1,2,5]

# alist = list(range(10000))
quick_sort(alist,0,len(alist)-1)
print(alist)





