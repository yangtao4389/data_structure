
def insert_sort(alist):
    # 插入排序
    # 第二个位置，开始向前插入，如果小于前一个元素，交换位置

    n = len(alist)
    for j in range(1,n):
    # i从1号下标开始，
        for i in range(j,0,-1):
            if alist[i] < alist[i-1]:
            # 如果小则交换
                alist[i], alist[i-1] = alist[i-1], alist[i]

alist =	[54,26,93,17,77,31,44,55,20]
# alist = list(range(10000))
insert_sort(alist)
print(alist)
