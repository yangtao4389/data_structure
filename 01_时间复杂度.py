# 如果	a+b+c=1000，	a^2+b^2=c^2（a,b,c	为⾃然数），
# 何求出所有a、b、c可能的组合?



# 第一种解法：直接循环遍历abc
'''
for a in range(0,1001):
    for b in range(0,1001):
        for c in range(0,1001):
            if a+b+c == 1000 and a**2+b**2 == c**2:
                print("a:{},b:{},c:{}".format(a,b,c))

# 第二种解法：
for a in range(0,1001):
    for b in range(0,1001):
        c = 1000-a-b
        if a**2 + b**2 == c**2:
            print("a:{},b:{},c:{}".format(a,b,c))
            '''



# 为了计算运行时间，引用timeit库
import timeit
def func1():
    for a in range(0, 1001):
        for b in range(0, 1001):
            for c in range(0, 1001):
                if a + b + c == 1000 and a ** 2 + b ** 2 == c ** 2:
                    print("a:{},b:{},c:{}".format(a, b, c))

def func2():
    for a in range(0, 1001):
        for b in range(0, 1001):
            c = 1000 - a - b
            if a ** 2 + b ** 2 == c ** 2:
                print("a:{},b:{},c:{}".format(a, b, c))

timer1 = timeit.Timer('func1()','from __main__ import func1')
print('第一种解法用的时间：',timer1.timeit(1))  #计算一次的时间  82s
# print(timer.timeit(10))  #计算10次的时间
# print(timer.timeit(1000)) #计算1k次的时间


timer2 = timeit.Timer('func2()','from __main__ import func2')
print('第二种解法用的时间：',timer2.timeit(1))  #计算一次的时间  0.83s


# 经过上面对比，发现不同的程序，运行的时间不一样。采用时间复杂度来重新度量。
# T(n) = O()  这个就代表时间复杂度 还有空间复杂度，S（n)
# 上面第一种的时间复杂度为：O（n^3)
# 第二种为O(n^2)

# 常见时间复杂度的对比：
# O(1) < O(logn) <O(n) < O(nlogn) < O(n**2)< O(n**3) < O(2**n) < O(n!) < O(n**n)


